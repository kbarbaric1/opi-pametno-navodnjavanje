# Pametno Navodnjavanje
## 🌱💧🌳
## Zahtjevi

Potreban je Arduino 1.8.13 ili noviji, a potrebne su i sljedeće biblioteke iz Arduino IDE Library Managera:

ESP32AsyncWebServer
ESPnow
ArduinoJson
sqlite3
TFT_eSPI

## Upotreba

Podesiti WiFi i ESPnow podatke u kodu. (WiFi SSID i lozinka, ESPnow MAC adresa i lozinka), WebServer.ino

Kompajlirati za ESP32 Dev Module.

Upload datoteka u data/ folderu na ESP32 SPIFFS Korištenjem ESP32 Sketch Data Upload alata. [arduino-esp32fs-plugin](https://github.com/me-no-dev/arduino-esp32fs-plugin)

Spojiti se na IP prikazan na TFT ekranu preko web preglednika.

## Demo

Demo web sučelja je dostupan ovdje: [Početna](WebServer/data/index.html)

